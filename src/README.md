# Software

Das Projekt beschreibt ein RFID Lesegerät welches über MQTT in einer IT-Infrastruktur für offene Werkstätten eingebunden werden kann, z. B. für die Freischaltung von Maschinen.

## Bibliotheken
Abgesehen von der 3DES-Bibliothek sind alle verwendete Bibliotheken sind über den Bibliothekenmanager von Arduino zu finden.
Die DES Bibliothek befindet sich [in github](https://github.com/Octoate/ArduinoDES)

## MQTT_Reader06
Aktuelle Version der Software für das Lesegerät. 




## Ultralight C
Software zum Beschreiben von Mifare Ultralight C Karten. Als Hardware reicht ein Arduino Uno und ein RC522 Modul. Die Daten werden über den Serialmonitor eingegeben.
Das Programm eignet sich zum Schreiben von Daten, Ändern des Schlüssels und Festlegen des verschlüsselten Datenbereiches auf der Karte.

### Struktur von Ultralight C Karten
Mifare Ultralight C karten sind, abgesehen von der Möglichkeit der Verschlüsselung relativ einfach strukturierte RFID-Karten.
Die Karten umfassen 144 Bytes beschreibbare Datenfelder die in je 4 Bytes pro Seite (Page) organisiert sind. Der Zugriff erfolgt immer über eine Seite (Page). Die ersten 4 Seiten (0-3) sind mit der UID, sogenannten Lock-Bits sowie mit einem OTP (one time programmable) Speicher belegt, dann folgen Datenfelder. Die letzten 8 Seiten sind ebenfalls mit Lock-bits, einen 16-bit Aufwärtszähler, die Zugriffskonfiguration und mit dem Zugriffsschlüssel belegt. Siehe hierzu das NPX Dokument [https://www.nxp.com/docs/en/data-sheet/MF0ICU2.pdf](https://www.nxp.com/docs/en/data-sheet/MF0ICU2.pdf).

Das erste Byte auf den Seiten der Zugriffskonfiguration legt fest wie sich die verschlüsselung der Karte auswirkt.
1. Es kann ab einer selbst festzulegende Seite an aufwärts der Zugriff auf die Daten eingeschränkt werden. d.h. ohne vorherige Authentifizierung ist kein / nur ein eingeschränkter Zugriff möglich. 
2. Es kann festgelegt werden, ob die gesperrte Seiten gar nicht sichtbar sind, oder nur einen Lesezugriff erlauben. 

Wichtig dabei ist, dass
1. die Seiten in denen der Schlüssel steht nicht ausgelesen werden können
2. das Überschreiben des Schlüssels nur verhindert werden kann, wenn die Seiten wo der Schlüssel steht (Seite 44 bis 47) auch durch die Zugriffkonfiguration gegen Schreiben gesperrt sind.

### Befehle
#### auth 
Der "auth" Befehl führt den 3DES Authorisierungsprozess mit der Karte durch. Dies ist erforderlich, wenn die Karte über einen Schlüssel gesichert ist, also die Zugriffskonfiguration bestimmte Seiten schützt (zum Beispiel den Schlüsselbereich, Seite 44-47).
+ Beispiel: "auth 49454D4B41455242214E4143554F5946" (hierbei handelt es sich um den Standard-Schlüssel für Mifare Ultralight C Karten)

#### newKey
Hiermit kann ein neuer Schlüssel definiert werden. Das Programm erwartet, dass man sich vorher mit dem aktuellen Schlüssel authorisiert hat. Der Schlüssel muss 16 Bytes (also 32 Hex-Zeichen) lang sein. Ist er kürzer, meckert das Programm, ist er länger, werden die letzten zeichen ignoriert.
+ Beispiel: "newKey 49454D4B41455242214E4143554F5946"

#### dump
Lesbare Datenfelder der Karte werden ausgegeben, inklusive der Information ob diese gesperrt sind und ob und wie diese durch eine Authentifizierung geschützt sind. Gesperrte Seiten können nciht mehr beschrieben werden. Sollte noch keine Authentifizierung stattgefunden haben und es Seiten geben, die gegen Lesen gesperrt sind, werden diese nicht angezeigt. Es sieht so aus, als hätte die Karte weniger Datenfelder. Nach einer erfolgreichen Authentifizierung sollten alle Datenfelder sichtbar sein.
+ Beispiel: "dump"

#### wchar
Beschreiben der Karte mit einem Text ab einer definierten Seite. Die Buchstaben des Textes werden 1:1 auf die Karte geschrieben. Die Zahl nach dem Befehl gibt die Startseite (dezimal) an.
+ Beispiel: "wchar 10 hello world"

#### whex
Beschreiben der Karte mit Hex-Werte ab einer definierten Seite. Die Zeichen des Textes werden jetzt als Hex-Zeichen interpretiert (00-FF -> 0x00 - 0xFF). Es werden immer zwei Zeichen für ein Byte benötigt. Es düfen nur Zahlen von 0-9 und Buchstaben von A-F bzw. a-f verwendet werden.
+ Beispiel: "whex 10 0123456789ABC" 

#### protect
Legt fest ab welcher Seite (Page) die Zugriffkonfiguration sich auswirkt. Eine Zahl 15 schränkt den Zugriff ab Seite 15 aufwärts ein. Wird hier die Zahl 48 eingegeben, ist die Karte nicht geschützt. ACHTUNG: Zahlen kleiner 2 können zu unerwartetem Verhalten führen, da vom Programm auf den unteren Seiten zugegriffen wird um das Vorhandensein einer Karte zu prüfen.
+ Beispiel: "protect 25" (ab Seite 25 aufwärts geschützt)

#### setpbit
Konfigurationsbit (protection Bit) mit dem festgelegt wird ob die geschützte Seiten nur Lesezugriff haben (1) oder weder Lese- noch Schreibzugriff haben (0). Mögliche Werte sind 1 oder 0
+ Beispiel: "setpbit 1" (nur Lesezugriff).

Mit diesen Befehlen sollte es möglich sein, eine Mifare Ultralight C Karte entsprechend zu konfigurieren um diese für den MQTT-Reader nutzen zu können.

### Möglicher Ablauf zum Personalisieren einer Karte

+ auth 49454D4B41455242214E4143554F5946 (authentifizieren)
+ dump (anzeigen was schon auf der Karte ist, hier werden dann auch die Lock- und Konfigurationsbits ausgelesen)
+ newKey 00112233445566778899AABBCCDDEEFF (neuen Schlüssel schreiben)
+ protect 30  (ab Seite 30 Zugriff einschränken)
+ setpbit 0 (weder Lese- noch Schreibzugriff)
+ wchar 4 Name der Werkstatt (damit dennoch gelesen werden kann zu welcher Werkstatt die Karte gehört)
+ dump (überprüfen ob alles auch richtig geschrieben ist)

!ACHTUNG!
Nach manchen Vorgängen meldet sich die Karte selber ab. D.h. hin und wieder muss man sich entweder neu authentifizieren oder den dump-Befehl ausführen, damit die Lock- und Konfigurationsbits gelesen werden.
