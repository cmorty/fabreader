# MQTT-RFID Reader

## Allgemein

Das Projekt beschreibt ein RFID Lesegerät welches über MQTT in einer IT-Infrastruktur für offene Werkstätten eingebunden werden kann, z. B. für die Freischaltung von Maschinen.

## Besonderheiten der Hardware
Die Hardware besteht Hauptsächlich aus standard "China"-Module. Diese werden mittel einer eigenen Adapterplatine (Siehe Eagle-Dateien) "verdrahtet". Damit das in ein Gehäuse mit brauchbare Abmßen passt werden die Platinen als Sandwich angeordnet, d.h. die Platinen liegen teilweise übereinander. Das macht den Zusammenbau an der eine oder andere Stelle etwas herausfordernd. Daher gibt es die detaillierte Bauanleitung.

## Das RC522 Modul
Damit das RC522 Modul überhaupt sinnvoll zum Einsatz kommen kann müssen die Induktionen L1 und L2 auf der Platinen gegen solche mit größerer Bauform ausgetauscht werden. Bei den verbauten Induktionen handelt es sich um zu klein dimensionierte Bauteilen. Geeignet sind folgende Induktionen: FERROCORE CW1008-2200. Nur dann werden Mifare Ultralight Typ C und DESFire karten zuverlässig gelesen. 